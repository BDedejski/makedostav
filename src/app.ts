import { PLATFORM } from 'aurelia-pal';
import { autoinject, bindable } from 'aurelia-framework';
import { RouteConfig, Router, NavigationInstruction } from 'aurelia-router';
import { HttpClient } from 'aurelia-fetch-client';

@autoinject
export class App {
  api;
  constructor(private router: Router, private httpClient: HttpClient, api) {
    this.api = api;
    httpClient.configure(config => {
      config.useStandardConfiguration();
    })
  }

  configureRouter(config, router) {
    config.titlee = 'Makedostav';

    const handleUnknownRoutes = (instruction: NavigationInstruction): RouteConfig => {
      return {
        route: '',
        moduleId: PLATFORM.moduleName('public/home'),
        nav:true
      };
    }
    config.mapUnknownRoutes(handleUnknownRoutes);

    config.map([
      {
        route: '', name: 'Home', title: 'Home',
        moduleId: PLATFORM.moduleName('public/home'),
        nav:true
      },
      {
        route: 'portfolio', name: 'Portfolio', title: 'Portfolio',
        moduleId: PLATFORM.moduleName('public/portfolio'),
        nav:true
      },
      {
        route: 'portfolio/:id', name: 'Portfolio', title: 'Portfolio',
        moduleId: PLATFORM.moduleName('public/portfolio-details'),
      },
      {
        route: 'about', name: 'About us', title: 'About us',
        moduleId: PLATFORM.moduleName('public/about'),
        nav:true
      },
      {
        route: 'contact', name: 'Contact', title: 'Contact',
        moduleId: PLATFORM.moduleName('public/contact'),
        nav:true
      },
      {
        route: 'join-us', name: 'Join us', title: 'Join us',
        moduleId: PLATFORM.moduleName('public/joinus'),
        nav:true
      },
      {
        route: 'join-us/:id', name: 'Join us', title: 'Join us',
        moduleId: PLATFORM.moduleName('public/job-details'),
        nav:false,
      },
      {
        route: 'admin', name: 'admin',
        moduleId: PLATFORM.moduleName('admin/admin-index', /* chunk: */ 'admin'),
        // settings: { auth: true }
      }
    ])

    this.router = router;
  }
  
}
