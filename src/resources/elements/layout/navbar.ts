import { autoinject, PLATFORM } from "aurelia-framework";
import { Router, NavigationInstruction, RouteConfig } from "aurelia-router";
import { HttpClient } from "aurelia-fetch-client";

@autoinject
export class Navbar {
  api;
  constructor(private router: Router, private httpClient: HttpClient, api) { 
    this.router = router;
  }

  attached(){
    // console.log(this.router)
  }
}
