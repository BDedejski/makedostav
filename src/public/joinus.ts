import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";

@autoinject
export class Joinus{

  constructor(private router:Router){}

  attached(){
    this.router.navigation[4].isActive = true;
  }
  detached(){
    this.router.navigation[4].isActive = false;
  }
}
