import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";

@autoinject
export class Portfolio{
  constructor(private router: Router){}

  attahced(){
    this.router.navigation[1].isActive = true;
  }
  detached(){
    this.router.navigation[1].isActive = false;
  }
}
