import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";

@autoinject
export class PortfolioDetails{
  constructor(private router: Router){}

  attached(){
    this.router.navigation[1].isActive = true;
    // baguetteBox.run('.tz-gallery');
  }
  detached(){
    this.router.navigation[1].isActive = false;
  }
}
