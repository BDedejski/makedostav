import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";


@autoinject
export class JobDetails {
  previousState: any;
  constructor(private router: Router) {
  }
  
  attached() {
    this.previousState = this.router.navigation[4].isActive;
    this.router.navigation[4].isActive = true;
  }
  detached() {
    this.router.navigation[4].isActive = this.previousState;
  }

}
