import { autoinject } from "aurelia-framework";
import * as $ from 'jquery';
import 'lib/sticky/sticky';
import 'lib/superfish/superfish';
import 'lib/magnific-popup/magnific-popup';
import * as WOW from 'lib/wow/wow';
import 'lib/owlcarousel/owl.carousel';
import 'lib/slick-1.8.1/slick-1.8.1/slick/slick';
import videojs from "video.js";

@autoinject
export class Home {
  navigation: any;
  videotry: HTMLElement;
  constructor() { }
  
  attached() {
    $(document).ready(function ($) {
      //FOR VIDEO CUSTOMIZING

      // var options = {};
      // var player = videojs('myplayer', options, function onPlayerReady() {
      //   videojs.log('Your player is ready!');

      //   // In this context, `this` is the player that was created by Video.js.
      //   this.play();
      //   // How about an event listener?
      //   this.on('ended', function() {
      //     videojs.log('Awww...over so soon?!');
      //   });
      // });

      // Back to top button
      $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
          $('.back-to-top').fadeIn('slow');
        } else {
          $('.back-to-top').fadeOut('slow');
        }
      });
      $('.back-to-top').click(function () {
        $('html, body').animate({
          scrollTop: 0
        }, 1500, 'easeInOutExpo');
        return false;
      });

      // Stick the header at top on scroll
      $("#header").sticky({
        topSpacing: 0,
        zIndex: '50'
      });
      // Initiate the wowjs animation library
      new WOW().init();

      // Initiate superfish on nav menu
      $('.navbar-nav').superfish({
        animation: {
          opacity: 'show'
        },
        speed: 400
      });
      // Porfolio - uses the magnific popup jQuery plugin
      $('.portfolio-popup').magnificPopup({
        type: 'image',
        removalDelay: 300,
        mainClass: 'mfp-fade',
        gallery: {
          enabled: true
        },
        zoom: {
          enabled: true,
          duration: 300,
          easing: 'ease-in-out',
          opener: function (openerElement) {
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
      });
      $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 4
          }
        }, {
          breakpoint: 520,
          settings: {
            slidesToShow: 3
          }
        }]
      });

    });
    $(".testimonials-carousel").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: false,
      dots: false,
      pauseOnHover: false,
      responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 1
        }
      }, {
        breakpoint: 520,
        settings: {
          slidesToShow: 1
        }
      }]
    });
  }
}
