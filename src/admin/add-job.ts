import { autoinject } from "aurelia-framework";


@autoinject
export class AddJob {

  allResponsibilities = [];
  responsibility: any;
  responsibilityField: HTMLElement;

  allRequirements = [];
  requirement: any;
  requirementField: HTMLElement;

  allBenefits = [];
  benefit:any;

  isSubmited:any;
  constructor() {
    this.isSubmited = false;
   }

  availablePositions = [
    { id: 0, name: '1' },
    { id: 1, name: '2' },
    { id: 2, name: '3' },
    { id: 3, name: '4' },
    { id: 3, name: '5+' }
  ];
  selectedPositions = null;

  workHours = [
    { id: 0, name: 'Full Time' },
    { id: 1, name: 'Part Time' }
  ];
  selectedWorkHours = null;



  addResponsibility() {
    this.allResponsibilities.push(this.responsibility);
    this.responsibility = "";
  }

  deleteResponsibility(responsibility) {
    const index = this.allResponsibilities.indexOf(responsibility, 0);
    if (index > -1) {
      this.allResponsibilities.splice(index, 1);
    }
  }

  addRequirement() {
    this.allRequirements.push(this.requirement);
    this.requirement = "";
  }

  deleteRequirement(requirement) {
    const index = this.allRequirements.indexOf(requirement, 0);
    if (index > -1) {
      this.allRequirements.splice(index, 1);
    }
  }


  addBenefit() {
    this.allBenefits.push(this.benefit);
    this.benefit = "";
  }

  deleteBenefit(benefit) {
    const index = this.allBenefits.indexOf(benefit, 0);
    if (index > -1) {
      this.allBenefits.splice(index, 1);
    }
  }

  addJob(){
    this.isSubmited = true;
    //TODO
  }
  canDeactivate() {
    if (this.isSubmited === false)
      return confirm('You have unsaved changes are you sure you wish to leave')

    return true;
  }
}
