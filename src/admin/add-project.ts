import { autoinject } from "aurelia-framework";


@autoinject
export class AddProject {
  selectedFiles: any;
  imageId: any;
  listOfImages: any;
  imagesIds: any;
  isSubmited: any;

  constructor() {
    this.listOfImages = [];
    this.imagesIds = [];
    this.isSubmited = false;
  }



  async upload() {
    let formData = new FormData();
    for (let i = 0; i < this.selectedFiles.length; i++) {
      formData.append(`files[${i}]`, this.selectedFiles[i]);
    }
    // let response = await this.api.upload("/images", formData);
    // this.imageId = response.imageId;
    this.listOfImages.push([`https://localhost:44319/api/images/${this.imageId}`, `${this.imageId}`]);
    this.imagesIds.push(this.imageId)
  }

  async deleteImage(image) {
    const index = this.imagesIds.indexOf(image[1]);
    if (index > -1) {
      // await this.api.delete(`/images/${image[1]}`) // to not store images that are uploaded before
      this.imagesIds.splice(index, 1);
      this.listOfImages.splice(index, 1);
    }
  }

  addProject() {
    //TODO 
    this.isSubmited = true;
  }
  
  canDeactivate() {
    if (this.isSubmited === false)
      return confirm('You have unsaved changes are you sure you wish to leave')

    return true;
  }
}
