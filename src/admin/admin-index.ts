import { autoinject, PLATFORM } from "aurelia-framework";
import { Router } from "aurelia-router";


@autoinject
export class AdminIndex {
  constructor(private router: Router) {
    console.log('adindex')
  }

  configureRouter(config, router) {
    config.map([
      {
        route: [''],
        redirect: 'jobs'
      },
      {
        route: 'jobs', name: 'jobs', icon: "fas fa-suitcase", title: "Jobs",
        moduleId: PLATFORM.moduleName('./jobs'),
        nav: true
      },
      {
        route: 'manage-portfolio', name: 'portfolio', icon: "fas fa-tasks", title: "Update Portfolio",
        moduleId: PLATFORM.moduleName('./manage-portfolio'),
        nav: true
      },
      {
        route: 'add-job', name: 'Add job', title: "Add job",
        moduleId: PLATFORM.moduleName('./add-job'),
      },
      {
        route: 'add-project', name: 'Add project', title: "Add project",
        moduleId: PLATFORM.moduleName('./add-project'),
      }
    ]);

    this.router = router;
  }
}
