import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";

@autoinject
export class ManagePortfolio{

  constructor(private router:Router){
    
  }

  navigateToProject(){
    this.router.navigate('/admin/add-project');
  }
}
