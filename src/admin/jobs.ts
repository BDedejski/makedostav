import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";

@autoinject
export class Jobs{
  constructor(private router: Router){
    console.log('in jobs');
  }

  navigateToAddJob(){
    this.router.navigate('/admin/add-job');
  }
}
